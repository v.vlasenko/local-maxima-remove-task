package com.epam.rd.autotasks.arrays;

import java.util.Arrays;

public class LocalMaximaRemove {

    public static void main(String[] args) {
        int[] array = new int[]{-3, 2, 4, 3, 5, 12, 8};
        //  {-3, 2, 4, 3, 5, 12, 8}  {18, 1, 3, 6, 7, -5}
        System.out.println(Arrays.toString(removeLocalMaxima(array)));
    }

    public static int[] removeLocalMaxima(int[] array) {
        int[] temp = new int[array.length];
        int size = 0;

        // validate if first number is not local maxima and put it in the temp array
        if (array[0] <= array[1]) {
            temp[size] = array[0];
            size++;
        }

        // iterate through array to find number that is not local maxima and put it into the temp array
        for (int i = 1; i < array.length-1; i++) {
            if (!(array[i] > array[i-1] &&
                  array[i] > array[i+1])) {
                temp[size] = array[i];
                size++;
            }
        }

        // validate if the last number is not local maxima and put it in the temp array
        if (array[array.length-1] <= array[array.length-2]) {
            temp[size] = array[array.length-1];
            size++;
        }

        int[] arrayToReturn = new int[size];
        System.arraycopy(temp,0,arrayToReturn,0,size);
        return arrayToReturn;

    }
}
